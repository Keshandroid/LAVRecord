package com.example.lav_record

import android.annotation.SuppressLint
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lav_record.Adapter.AudioItemAdapter
import com.example.lav_record.Database.DatabaseHelper
import com.example.lav_record.Model.Recording
import com.example.lav_record.databinding.ActivityAudioListBinding
import java.io.File
import java.io.IOException

class AudioListActivity : AppCompatActivity() {

    private var mediaPlayer: MediaPlayer? = null
    private var lastProgress = 0
    private val mHandler = Handler()
    private var isPlaying = false
    private var last_index = -1
    private var myAdapter: AudioItemAdapter? = null
    var helper: DatabaseHelper? = null

    private lateinit var binding: ActivityAudioListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAudioListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        helper = DatabaseHelper(this)
        getAllRecordings()
    }

    /*override fun onClickPlay(
        view: View,
        record: Recording,
        recordingList: ArrayList<Recording>,
        position: Int
    ) {
        playRecordItem(view, record, recordingList, position)
    }*/

    private fun playRecordItem(
        view: View,
        recordItem: Recording,
        recordingList: ArrayList<Recording>,
        position: Int
    ) {
        val recording = recordingList[position]

        if (isPlaying) {
            stopPlaying()
            if (position == last_index) {
                recording.isPlaying = false

                stopPlaying()
                myAdapter!!.notifyItemChanged(position)
            } else {
                markAllPaused(recordingList)
                recording.isPlaying = true
                myAdapter!!.notifyItemChanged(position)
                startPlaying(recording.uri, recordItem, view.findViewById(R.id.seekbar), position)
                last_index = position
            }
            seekUpdate(view)
            manageSeekBar(view.findViewById(R.id.seekbar))
        } else {
            if (recording.isPlaying) {
                recording.isPlaying = false
                stopPlaying()
            } else {
                startPlaying(recording.uri, recordItem, view.findViewById(R.id.seekbar), position)
                recording.isPlaying = true
                view.findViewById<SeekBar>(R.id.seekbar).max = mediaPlayer!!.duration
            }
            myAdapter!!.notifyItemChanged(position)
            last_index = position
        }

        manageSeekBar(view.findViewById(R.id.seekbar))
    }

    private fun getAllRecordings() {
        //  this.deleteDatabase(DATABASE_NAME)

        val recordArrayList = ArrayList<Recording>()
        val root = Environment.getExternalStorageDirectory()

        try {
            helper?.allAudio

        } catch (e: IllegalStateException) {
            Log.d("Error", e.toString())
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
            Log.d("error", e.message.toString())
        }

        //val roott = Environment.getExternalStorageDirectory()

        val pathh =
            root.absolutePath + File.separator + Environment.DIRECTORY_DCIM + File.separator + "AUDIORECORD"

        Log.d("pathh", pathh)

        val filepath = (Environment.getExternalStorageDirectory()
            .toString() + File.separator + Environment.DIRECTORY_DCIM + File.separator + "AUDIORECORD")

        val directory = File(pathh)

        val files = directory.listFiles()

        if (files != null) {

            for (i in files.indices) {

                val fileName = files[i].name

                val record: List<Recording> = helper?.allAudio as List<Recording>

                recordArrayList.clear()

                for (cn in record) {
                    val file = files[i].name

                    val recordingUri =
                        root.absolutePath + "/" + Environment.DIRECTORY_DCIM + File.separator + "AUDIORECORD/" + file

                    val uri = cn.uri
                    val name = cn.fileName
                    val date = cn.date
                    val time = cn.time

                    Recording(uri, name, false, date, time).let {
                        recordArrayList.add(it)
                    }
                }
            }
            //  Log.d("LocalData", "ArrayList : " + recordArrayList.size)

        }

        binding.rvAudio.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        myAdapter = AudioItemAdapter(recordArrayList,this)
        //  binding.rvAudio.setHasFixedSize(true)

        //myAdapter!!.setListener(this)
        binding.rvAudio.adapter = myAdapter
    }


    @SuppressLint("CutPasteId")
    private fun seekUpdate(itemView: View) {
        if (mediaPlayer != null) {
            val mCurrentPosition = mediaPlayer!!.currentPosition
            itemView.findViewById<SeekBar>(R.id.seekbar).max = mediaPlayer!!.duration
            itemView.findViewById<SeekBar>(R.id.seekbar).progress = mCurrentPosition
            lastProgress = mCurrentPosition
        }
        mHandler.postDelayed(Runnable {
            seekUpdate(itemView)
        }, 100)
    }

    private fun manageSeekBar(seekBar: SeekBar?) {
        seekBar!!.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                if (mediaPlayer != null && fromUser) {
                    mediaPlayer!!.seekTo(progress)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {

            }
        })
    }

    private fun stopPlaying() {
        try {
            mediaPlayer?.stop()
            //   mediaPlayer!!.reset()
            //  mediaPlayer!!.release()
            mediaPlayer = null

            //mediaPlayer!!.release()

        } catch (e: Exception) {
            e.printStackTrace()
        }

        mediaPlayer = null
        isPlaying = false
    }

    private fun startPlaying(uri: String?, audio: Recording?, seekBar: SeekBar?, position: Int) {
        mediaPlayer = MediaPlayer()
        try {
            mediaPlayer!!.setDataSource(uri)
            //mediaPlayer!!.prepareAsync()

            try {
                mediaPlayer!!.prepare()
            } catch (e: IllegalStateException) {
                Log.d("Error", e.toString())
                e.printStackTrace()
            } catch (e: IOException) {
                Log.d("Error", e.toString())
                e.printStackTrace()
            }

            try {
                mediaPlayer!!.start()
            } catch (e: java.lang.IllegalStateException) {
                e.printStackTrace()
                Log.d("Error", e.message + "")
            }
        } catch (e: IOException) {
            Log.d("uri", e.toString())
            //Log.e("LOG_TAG", e.toString())
            // Log.e("LOG_TAG1", e.message.toString())
        }
        //showing the pause button
        seekBar!!.max = mediaPlayer!!.duration
        isPlaying = true

        mediaPlayer!!.setOnCompletionListener(MediaPlayer.OnCompletionListener {
            audio!!.isPlaying = false
            myAdapter!!.notifyItemChanged(position)
        })
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun markAllPaused(recordingList: ArrayList<Recording>) {
        for (i in recordingList.indices) {
            recordingList[i].isPlaying = false
            recordingList[i] = recordingList[i]
        }
        myAdapter!!.notifyDataSetChanged()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        stopPlaying()
    }

}




