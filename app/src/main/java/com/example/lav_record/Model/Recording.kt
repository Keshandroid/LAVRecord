package com.example.lav_record.Model

import java.io.Serializable

class Recording(
    var uri: String,
    var fileName: String,
    var isPlaying: Boolean,
    var date : String,
    var time : String

) : Serializable