package com.example.lav_record.Adapter

import android.app.Activity
import android.media.MediaPlayer
import android.media.MediaPlayer.OnCompletionListener
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.lav_record.Model.Recording
import com.example.lav_record.R


class AudioItemAdapter(audioItems: List<Recording>,val context : Activity) : RecyclerView.Adapter<AudioItemAdapter.AudioItemsViewHolder>() {
    private var mediaPlayer: MediaPlayer? = null
    private val audioItems: List<Recording>
    private var currentPlayingPosition: Int
    private val seekBarUpdater: SeekBarUpdater
    private var playingHolder: AudioItemsViewHolder? = null

    init {
        this.audioItems = audioItems
        currentPlayingPosition = -1
        seekBarUpdater = SeekBarUpdater()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AudioItemsViewHolder {
        return AudioItemsViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_listrecord, parent, false)
        )
    }

    override fun onBindViewHolder(holder: AudioItemsViewHolder, position: Int) {

        holder.audioDateTime.text = audioItems.get(position).date
        holder.audioTime.text = audioItems.get(position).time
        holder.audioName.text = audioItems.get(position).fileName

        if (position == currentPlayingPosition) {
            playingHolder = holder
            updatePlayingView()
        } else {
            updateNonPlayingView(holder)
        }
    }

    override fun onViewRecycled(holder: AudioItemsViewHolder) {
        super.onViewRecycled(holder)
        if (currentPlayingPosition == holder.adapterPosition) {
            updateNonPlayingView(playingHolder)
            playingHolder = null
        }
    }

    private fun updateNonPlayingView(holder: AudioItemsViewHolder?) {
        holder!!.sbProgress.removeCallbacks(seekBarUpdater)
        holder.sbProgress.isEnabled = false
        holder.sbProgress.progress = 0
        holder.ivPlayPause.setImageResource(R.drawable.play)
    }

    private fun updatePlayingView() {
        playingHolder!!.sbProgress.max = mediaPlayer!!.duration
        playingHolder!!.sbProgress.progress = mediaPlayer!!.currentPosition
        playingHolder!!.sbProgress.isEnabled = true
        if (mediaPlayer!!.isPlaying) {
            playingHolder!!.sbProgress.postDelayed(seekBarUpdater, 100)
            playingHolder!!.ivPlayPause.setImageResource(R.drawable.push)
        } else {
            playingHolder!!.sbProgress.removeCallbacks(seekBarUpdater)
            playingHolder!!.ivPlayPause.setImageResource(R.drawable.play)
        }
    }

    fun stopPlayer() {
        if (null != mediaPlayer) {
            releaseMediaPlayer()
        }
    }

    private inner class SeekBarUpdater : Runnable {
        override fun run() {
            if (null != playingHolder) {
                playingHolder!!.sbProgress.progress = mediaPlayer!!.currentPosition
                playingHolder!!.sbProgress.postDelayed(this, 100)
            }
        }
    }

    override fun getItemCount(): Int {
        return audioItems.size
    }

    inner class AudioItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener, OnSeekBarChangeListener {
        var sbProgress: SeekBar
        var ivPlayPause: ImageView
        var audioTime: TextView
        var audioDateTime: TextView
        var audioName: TextView

        init {
            ivPlayPause = itemView.findViewById(R.id.btn_play) as ImageView
            ivPlayPause.setOnClickListener(this)
            sbProgress = itemView.findViewById(R.id.seekbar)
            sbProgress.setOnSeekBarChangeListener(this)

            audioTime = itemView.findViewById<TextView>(R.id.AudioTime)
            audioDateTime = itemView.findViewById<TextView>(R.id.AudioDateTime)
            audioName = itemView.findViewById<TextView>(R.id.AudioName)



        }

        override fun onClick(v: View?) {
            if (adapterPosition == currentPlayingPosition) {
                if (mediaPlayer!!.isPlaying) {
                    mediaPlayer!!.pause()
                } else {
                    mediaPlayer!!.start()
                }
            } else {
                currentPlayingPosition = adapterPosition
                if (mediaPlayer != null) {
                    if (null != playingHolder) {
                        updateNonPlayingView(playingHolder)
                    }
                    mediaPlayer!!.release()
                }
                playingHolder = this
//                startMediaPlayer(audioItems[currentPlayingPosition].audioResId)

                startMediaPlayer(audioItems[currentPlayingPosition].uri)
            }
            updatePlayingView()
        }

        override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
            if (fromUser) {
                mediaPlayer!!.seekTo(progress)
            }
        }

        override fun onStartTrackingTouch(seekBar: SeekBar) {}
        override fun onStopTrackingTouch(seekBar: SeekBar) {}
    }

    private fun startMediaPlayer(audioResId: String) {
        mediaPlayer = MediaPlayer.create(context, Uri.parse(audioResId))
        mediaPlayer?.setOnCompletionListener(OnCompletionListener { releaseMediaPlayer() })
        mediaPlayer?.start()
    }

    private fun releaseMediaPlayer() {
        if (null != playingHolder) {
            updateNonPlayingView(playingHolder)
        }
        mediaPlayer!!.release()
        mediaPlayer = null
        currentPlayingPosition = -1
    }
}