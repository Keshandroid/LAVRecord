package com.example.lav_record.Adapter

import android.media.MediaPlayer
import android.os.Handler
import android.transition.TransitionManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.SeekBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.lav_record.Model.Recording
import com.example.lav_record.R

class AdapterNew(private val recordList: ArrayList<Recording>) :
    RecyclerView.Adapter<AdapterNew.ViewHolder>() {

    var clickPosition = -1


    private var onClickListener: OnClickListener? = null

    fun setListener(onClickListener: OnClickListener) {
        this.onClickListener = onClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_listrecord, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.bindItems(recordList, onClickListener, position)

        val recording: Recording = recordList.get(position)



        holder.AudioDateTime?.text = recording.date
        holder.AudioTime?.text = recording.time
        holder.AudioName?.text = recording.fileName
//        holder.setIsRecyclable(false)

        holder.imgViewPlay.visibility = View.VISIBLE
        holder.imgViewPlay.setOnClickListener(View.OnClickListener {
            if (onClickListener != null) {

                clickPosition = position;
                notifyDataSetChanged();




                //onClickListener!!.onClickPlay(holder.itemView, recording, recordList, position) // original


            }
        })

        if(clickPosition == position){
            holder.imgViewPlay.setVisibility(View.INVISIBLE);
            holder.imgViewPause.setVisibility(View.VISIBLE);
        }

        holder.imgViewPause.setOnClickListener(View.OnClickListener {
            if (onClickListener != null) {

                clickPosition = position;
                notifyDataSetChanged();


                //onClickListener!!.onClickPlay(holder.itemView, recording, recordList, position) // original


            }
        })



    }

    override fun getItemCount(): Int {
        return recordList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var mediaPlayer: MediaPlayer? = null
        private var lastProgress = 0
        private val mHandler = Handler()
        val AudioTime = itemView.findViewById<TextView>(R.id.AudioTime)
        val AudioDateTime = itemView.findViewById<TextView>(R.id.AudioDateTime)
        var AudioName = itemView.findViewById<TextView>(R.id.AudioName)
        val seekBar = itemView.findViewById<SeekBar>(R.id.seekbar)
        val rl_play = itemView.findViewById<RelativeLayout>(R.id.rl_play)
        val imgViewPause = itemView.findViewById<ImageView>(R.id.btn_pause)
        val imgViewPlay = itemView.findViewById<ImageView>(R.id.btn_play)



        fun bindItems(
            recordingList: ArrayList<Recording>,
            onClickListener: OnClickListener?,
            position: Int
        ) {


            /*if (recording.isPlaying) {
                imgViewPlay.setImageResource(R.drawable.push)
                TransitionManager.beginDelayedTransition(itemView as ViewGroup)
                //rl_play.visibility = View.VISIBLE
                //seekBar.visibility = View.VISIBLE
                seekUpdate(itemView)

            } else {
                imgViewPlay.setImageResource(R.drawable.play)
                TransitionManager.beginDelayedTransition(itemView as ViewGroup)
                //seekBar.visibility = View.GONE
                //rl_play.visibility = View.GONE
                manageSeekBar(seekBar)
            }*/

            manageSeekBar(seekBar)




        }

        private fun manageSeekBar(seekBar: SeekBar?) {
            seekBar!!.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    if (mediaPlayer != null && fromUser) {
                        mediaPlayer!!.seekTo(progress)
                    }
                }

                override fun onStartTrackingTouch(seekBar: SeekBar) {

                }

                override fun onStopTrackingTouch(seekBar: SeekBar) {

                }
            })
        }

        private var runnable: Runnable = Runnable { seekUpdate(itemView) }

        private fun seekUpdate(itemView: View) {
            if (mediaPlayer != null) {
                val mCurrentPosition = mediaPlayer!!.currentPosition
                // itemView.seekBar.max = mediaPlayer!!.duration
                // itemView.seekBar.progress = mCurrentPosition
                itemView.findViewById<SeekBar>(R.id.seekbar).max = mediaPlayer!!.duration
                itemView.findViewById<SeekBar>(R.id.seekbar).progress = mCurrentPosition
                lastProgress = mCurrentPosition
            }
            mHandler.postDelayed(runnable, 100)
        }
    }

    interface OnClickListener {
        fun onClickPlay(
            view: View,
            record: Recording,
            recordingList: ArrayList<Recording>,
            position: Int
        )
    }
}