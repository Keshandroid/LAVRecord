package com.example.lav_record.Adapter

import android.media.MediaPlayer
import android.os.Handler
import android.transition.TransitionManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.SeekBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.lav_record.Model.Recording
import com.example.lav_record.R

class Adapter(private val recordList: ArrayList<Recording>) :
    RecyclerView.Adapter<Adapter.ViewHolder>() {

    private var onClickListener: OnClickListener? = null

    fun setListener(onClickListener: OnClickListener) {
        this.onClickListener = onClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_listrecord, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(recordList, onClickListener, position)

        holder.AudioDateTime?.text = recordList[position].date
        holder.AudioTime?.text = recordList[position].time
        holder.AudioName?.text = recordList[position].fileName
        holder.setIsRecyclable(false)

    }

    override fun getItemCount(): Int {
        return recordList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var mediaPlayer: MediaPlayer? = null
        private var lastProgress = 0
        private val mHandler = Handler()
        val AudioTime = itemView.findViewById<TextView>(R.id.AudioTime)
        val AudioDateTime = itemView.findViewById<TextView>(R.id.AudioDateTime)
        var AudioName = itemView.findViewById<TextView>(R.id.AudioName)

        fun bindItems(
            recordingList: ArrayList<Recording>,
            onClickListener: OnClickListener?,
            position: Int
        ) {
            val recording: Recording = recordingList[adapterPosition]
            val tvRecordName = itemView.findViewById<TextView>(R.id.AudioName)
            val imgViewPlay = itemView.findViewById<ImageView>(R.id.btn_play)
            val imgViewPause = itemView.findViewById<ImageView>(R.id.btn_pause)
            val seekBar = itemView.findViewById<SeekBar>(R.id.seekbar)
            tvRecordName.text = recording.fileName
            val rl_play = itemView.findViewById<RelativeLayout>(R.id.rl_play)


            /*if (recording.isPlaying) {
                imgViewPlay.setImageResource(R.drawable.push)
                TransitionManager.beginDelayedTransition(itemView as ViewGroup)
                //rl_play.visibility = View.VISIBLE
                //seekBar.visibility = View.VISIBLE
                seekUpdate(itemView)

            } else {
                imgViewPlay.setImageResource(R.drawable.play)
                TransitionManager.beginDelayedTransition(itemView as ViewGroup)
                //seekBar.visibility = View.GONE
                //rl_play.visibility = View.GONE
                manageSeekBar(seekBar)
            }*/

            manageSeekBar(seekBar)

            imgViewPlay.setOnClickListener(View.OnClickListener {
                if (onClickListener != null) {

                    onClickListener.onClickPlay(itemView, recording, recordingList, adapterPosition)
                    // val name = recordingList[position].fileName
                    //Log.d("AudioClick", "" + position)
                    //Log.d("AudioClick", "" + recordingList[position].fileName)
                    //Log.d("AutoClick", "" + recordingList[position].uri)

                }
            })


        }

        private fun manageSeekBar(seekBar: SeekBar?) {
            seekBar!!.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    if (mediaPlayer != null && fromUser) {
                        mediaPlayer!!.seekTo(progress)
                    }
                }

                override fun onStartTrackingTouch(seekBar: SeekBar) {

                }

                override fun onStopTrackingTouch(seekBar: SeekBar) {

                }
            })
        }

        private var runnable: Runnable = Runnable { seekUpdate(itemView) }

        private fun seekUpdate(itemView: View) {
            if (mediaPlayer != null) {
                val mCurrentPosition = mediaPlayer!!.currentPosition
                // itemView.seekBar.max = mediaPlayer!!.duration
                // itemView.seekBar.progress = mCurrentPosition
                itemView.findViewById<SeekBar>(R.id.seekbar).max = mediaPlayer!!.duration
                itemView.findViewById<SeekBar>(R.id.seekbar).progress = mCurrentPosition
                lastProgress = mCurrentPosition
            }
            mHandler.postDelayed(runnable, 100)
        }
    }

    interface OnClickListener {
        fun onClickPlay(
            view: View,
            record: Recording,
            recordingList: ArrayList<Recording>,
            position: Int
        )
    }
}