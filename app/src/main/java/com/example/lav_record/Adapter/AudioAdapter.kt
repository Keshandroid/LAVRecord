package com.example.lav_record.Adapter

import android.annotation.SuppressLint
import android.media.MediaPlayer
import android.os.Handler
import android.transition.TransitionManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.SeekBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.lav_record.Model.Recording
import com.example.lav_record.R

class AudioAdapter(
    private val recordList: ArrayList<Recording>
) : RecyclerView.Adapter<AudioAdapter.ViewHolder>() {


    private var onClickListener: OnClickListener? = null

    fun setListener(onClickListener: OnClickListener) {
        this.onClickListener = onClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_listrecord, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        holder.bindItems(recordList, onClickListener, position)
        holder.AudioName?.text = recordList[position].fileName
        holder.AudioDateTime?.text = recordList[position].date
        holder.AudioTime?.text = recordList[position].time
        holder.setIsRecyclable(false)


        //dbManager?.update(position.toLong(), date, time)

    }

    override fun getItemCount(): Int {
        return recordList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var mediaPlayer: MediaPlayer? = null
        private var lastProgress = 0
        private val mHandler = Handler()
        var seekBar: SeekBar? = null
        val AudioTime = itemView.findViewById<TextView>(R.id.AudioTime)
        val AudioDateTime = itemView.findViewById<TextView>(R.id.AudioDateTime)
        var AudioName = itemView.findViewById<TextView>(R.id.AudioName)

        fun bindItems(
            recordingList: ArrayList<Recording>,
            onClickListener: OnClickListener?,
            position: Int,
        ) {

            val recording: Recording = recordingList[adapterPosition]

            val tvRecordName = itemView.findViewById<TextView>(R.id.AudioName)

            val imgViewPlay = itemView.findViewById<ImageView>(R.id.btn_play)

            val rl_play = itemView.findViewById<RelativeLayout>(R.id.rl_play)

            tvRecordName.text = recording.fileName
            seekUpdate(itemView)

            if (recording.isPlaying) {
                imgViewPlay.setImageResource(R.drawable.push)
                TransitionManager.beginDelayedTransition(itemView as ViewGroup)
                seekBar?.visibility = View.VISIBLE
                seekUpdate(itemView)

                rl_play.visibility = View.VISIBLE
            } else {

                imgViewPlay.setImageResource(R.drawable.play)
                TransitionManager.beginDelayedTransition(itemView as ViewGroup)
                rl_play.visibility = View.GONE
            }

            manageSeekBar(seekBar)

            imgViewPlay.setOnClickListener(View.OnClickListener {

                // val name = recordingList[position].fileName
                //Log.d("AudioClick", "" + position)
               // Log.d("AudioClick", "" + recordingList[position].fileName)
                //Log.d("AudioClick", "" + recordingList[position].uri)

                onClickListener?.onClickPlay(itemView, recording, recordingList, adapterPosition)
            })
        }

        private fun manageSeekBar(seekBar: SeekBar?) {
            seekBar!!.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    if (mediaPlayer != null && fromUser) {
                        mediaPlayer!!.seekTo(progress)
                    }
                }

                override fun onStartTrackingTouch(seekBar: SeekBar) {

                }

                override fun onStopTrackingTouch(seekBar: SeekBar) {

                }
            })
        }

        private var runnable: Runnable = Runnable {
            seekUpdate(itemView)
        }


        private fun seekUpdate(itemView: View) {
            if (mediaPlayer != null) {
                val mCurrentPosition = mediaPlayer!!.currentPosition
                itemView.findViewById<SeekBar>(R.id.seekbar).max = mediaPlayer?.duration!!
                itemView.findViewById<SeekBar>(R.id.seekbar).progress = mCurrentPosition
                lastProgress = mCurrentPosition
            }
            mHandler.postDelayed(runnable, 100)
        }
    }

    interface OnClickListener {

        fun onClickPlay(
            view: View,
            record: Recording,
            recordingList: ArrayList<Recording>,
            position: Int
        )
    }
}