package com.example.lav_record

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaRecorder
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.SystemClock
import android.text.format.DateFormat
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.lav_record.Database.DatabaseHelper
import com.example.lav_record.Model.Recording
import com.example.lav_record.databinding.ActivityAudioBinding
import com.example.lav_record.utils.Constants
import com.example.lav_record.utils.StoreUserData
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class AudioActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityAudioBinding
    private val RECORD_AUDIO_REQUEST_CODE = 101
    var recorder: MediaRecorder? = null
    private val AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4"
    private val currentFormat = 0
    private var dateFormat: SimpleDateFormat? = null
    private val AUDIO_RECORD = "AUDIORECORD"
    private val file_exts = arrayOf(AUDIO_RECORDER_FILE_EXT_MP4)
    var elapsed = 0
    var helper: DatabaseHelper? = null
    var recordingItem: Recording? = null
    var fileName: String? = null
    val filepath = (Environment.getExternalStorageDirectory()
        .toString() + File.separator + Environment.DIRECTORY_DCIM + File.separator + "AUDIORECORD")
    val df: SimpleDateFormat = SimpleDateFormat("HH:mm")
    val dateToday: String = df.format(Calendar.getInstance().time)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAudioBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setListener()
        helper = DatabaseHelper(this)
        elapsed = 0

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getPermissionToRecordAudio()
        }


    }

    private fun setListener() {

        binding.btnStart.setOnClickListener(this)
        binding.btnStop.setOnClickListener(this)
        binding.textStartRecord.setOnClickListener(this)
        binding.textStopRecord.setOnClickListener(this)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun getPermissionToRecordAudio() {
        // 1) Use the support library version ContextCompat.checkSelfPermission(...) to avoid checking the build version since Context.checkSelfPermission(...) is only available in Marshmallow
        // 2) Always check for permission (even if permission has already been granted) since the user can revoke permissions at any time through Settings
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED
            || ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
            || ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), RECORD_AUDIO_REQUEST_CODE
            )
        }
    }

    // Callback with the request from calling requestPermissions(...)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == RECORD_AUDIO_REQUEST_CODE) {
            if (grantResults.size == 3 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                //Toast.makeText(this, "Record Audio permission granted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(
                    this,
                    "You must give permissions to use this app. App is exiting.",
                    Toast.LENGTH_SHORT
                ).show()
                finishAffinity()
            }
        }
    }

    override fun onClick(view: View?) {

        when (view?.id) {
            R.id.btnStart -> {

                binding.rlStart.visibility = View.GONE
                binding.rlStop.visibility = View.VISIBLE

                startRecord()
            }
            R.id.btnStop -> {
                stopRecord()
                //   onBackPressed()
            }

            R.id.textStartRecord -> {
                binding.rlStart.visibility = View.GONE
                binding.rlStop.visibility = View.VISIBLE

                binding.simpleChronometer.setBase(SystemClock.elapsedRealtime());
                binding.simpleChronometer.stop();

                startRecord()
            }
            R.id.textStopRecord -> {

                binding.rlStart.visibility = View.VISIBLE
                binding.rlStop.visibility = View.GONE

                stopRecord()


            }
        }
    }

    private fun getFilename(): String {
        //String filepath = Environment.getExternalStorageDirectory().getPath();

        fileName = "Audio_"
        val file = File(filepath)
        if (!file.exists()) {
            file.mkdirs()
        }

        val storedata = StoreUserData(this)


        storedata.setString(
            Constants.NAME,
            fileName + System.currentTimeMillis().toString()
                .substring(0, 10) + file_exts[currentFormat]
        )
        storedata.setString(
            Constants.audioPath,
            file.absolutePath + "/" + fileName + System.currentTimeMillis() + file_exts[currentFormat]
        )

        return file.absolutePath + "/" + fileName + System.currentTimeMillis() + file_exts[currentFormat]


    }


    @SuppressLint("SimpleDateFormat")
    private fun startRecord() {
        val storedata = StoreUserData(this)
         binding.simpleChronometer.base = SystemClock.elapsedRealtime() // new
//        binding.simpleChronometer.base = SystemClock.elapsedRealtime() - elapsed //old
        // binding.simpleChronometer.format = "Time (%s)"
        binding.simpleChronometer.start()


        val d = Date()   //get the current time.
        val date: CharSequence = DateFormat.format("MMMM d, yyyy ", d.time)

        storedata.setString(Constants.DATE, date.toString())
        recorder = MediaRecorder()

        recorder?.setAudioSource(MediaRecorder.AudioSource.MIC)
        recorder?.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
        recorder?.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)

        recorder?.setOutputFile(getFilename())

        try {
            recorder!!.prepare()
        } catch (e: IllegalStateException) {
            Log.d("Error", e.toString())
            e.printStackTrace()
        } catch (e: IOException) {
            Log.d("Error", e.toString())
            e.printStackTrace()
        }

        try {
            recorder!!.start()
        } catch (e: java.lang.IllegalStateException) {
            e.printStackTrace()
            Log.d("Error", e.message + "")
        }
    }

    private fun stopRecord() {
        if (null != recorder) {

            recorder!!.stop()
            elapsed = (SystemClock.elapsedRealtime() - binding.simpleChronometer.base).toInt()
            val time = binding.simpleChronometer.text

            binding.simpleChronometer.stop()

            val dateFormat = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
            val date = dateFormat.format(Date())

            val name = "Audio_" + System.currentTimeMillis() + file_exts[currentFormat]

            val AudioName: String = System.currentTimeMillis().toString().substring(0, 10)

            val storedata = StoreUserData(this)

            val data = storedata.getString(Constants.audioPath)
            val NAME = storedata.getString(Constants.NAME)

            val root = Environment.getExternalStorageDirectory()
            val fileName =
                root.absolutePath + "/" + Environment.DIRECTORY_DCIM + File.separator + "AUDIORECORD/" + name

            val pathh =
                root.absolutePath + File.separator + Environment.DIRECTORY_DCIM + File.separator + "AUDIORECORD"
            val directory = File(pathh)

            Log.d("timeRecordStop", time.toString())

            recordingItem = Recording(data, NAME, false, date, time.toString())
            helper?.addRecording(recordingItem)

            recorder!!.reset()
            recorder!!.release()
            recorder = null

            val i = Intent(this, AudioListActivity::class.java)
            startActivity(i)

        }
    }


    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
}