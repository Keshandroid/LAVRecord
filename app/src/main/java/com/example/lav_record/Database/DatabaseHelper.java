package com.example.lav_record.Database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.lav_record.Model.Recording;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    public Context context;
    public static final String DATABASE_NAME = "AudioRecord.db";
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE_NAME = "record_table";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_URI = "uri";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_TIME_ADDED = "time_added";
    public SQLiteDatabase database;

    public static final String COMA_SEP = ",";

    public static final String SQLITE_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
            + " (" + " id INTEGER PRIMARY KEY " +
            "AUTOINCREMENT " + COMA_SEP +
            COLUMN_URI + " TEXT " + COMA_SEP +
            COLUMN_NAME + " TEXT " + COMA_SEP +
            COLUMN_DATE + " TEXT " + COMA_SEP +
            COLUMN_TIME_ADDED + " INTEGER " + ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(SQLITE_CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

    }

    public void clearDatabase(String TABLE_NAME) {
        String clearDBQuery = "DELETE FROM " + TABLE_NAME;
        database.execSQL(clearDBQuery);
    }

    public boolean addRecording(Recording recording) {

        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_URI, recording.getUri());
            contentValues.put(COLUMN_NAME, recording.getFileName());
            contentValues.put(COLUMN_DATE, recording.getDate());
            contentValues.put(COLUMN_TIME_ADDED, recording.getTime());
            db.insert(TABLE_NAME, null, contentValues);

            return true;
        } catch (Exception e) {

            e.printStackTrace();
            return false;
        }


    }

    public List<Recording> getAllAudio() {

        ArrayList<Recording> arrayList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            //      Log.d("SQLITE_LOG", "cursor...");

            // cursor.moveToFirst();

            // Log.d("SQLITE_LOG", "Index : " + cursor.getString(0))

            while (cursor.moveToNext()) {
                String uri = cursor.getString(1);
                String name = cursor.getString(2);
                String date = cursor.getString(3);
                String timeAdded = cursor.getString(4);

                Recording recording = new Recording(uri, name, false, date, timeAdded);
                arrayList.add(recording);
            }

            cursor.close();


        }

        return arrayList;

    }


    public Cursor getInformation(SQLiteDatabase db) {
        Cursor cursor;
        String[] projections = {COLUMN_DATE, COLUMN_TIME_ADDED};
        cursor = db.query(TABLE_NAME, projections, null, null, null, null, null);
        return cursor;
    }
}
